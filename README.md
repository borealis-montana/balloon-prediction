# balloon-prediction

Python code for balloon predictions based on weather forecasts

# Installation

This installation guide expects that you have Anaconda install and are running on a Unix system (Linux or Mac)

```
conda create --name pyn_env --channel conda-forge pynio python=2.7
source activate pyn_env
```

The `conda create` command will set up the create the python environment necessary for this prediction code
The `source activate` enable the use of this python environment. This command will need to be run every time you open a new terminal unless you set `pyn_env` as the default python environment.